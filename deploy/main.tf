terraform {
  required_providers {
    google = {
      source  = "hashicorp/google"
      version = "3.52.0"
    }

  }
}

provider "google" {
  credentials = file(var.google_application_credentials)
  project     = var.project
  region      = var.region
  zone        = var.zone
}
