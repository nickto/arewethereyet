variable "project" {}

variable "google_application_credentials" {
  type = string
}

variable "region" {
  default = "europe-west3"
}

variable "zone" {
  default = "europe-west3-a"
}

variable "domain" {}
