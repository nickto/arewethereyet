resource "google_app_engine_application" "app" {
  project     = var.project
  location_id = var.region
}

resource "google_app_engine_domain_mapping" "default" {
  domain_name = var.domain

  ssl_settings {
    ssl_management_type = "AUTOMATIC"
  }
}