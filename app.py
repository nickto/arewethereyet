from flask import Flask, render_template
from colour import Color
import requests
from datetime import datetime

app = Flask(__name__)

BACKEND_HOST = "https://data.covid.nickto.net"


def get_proportion():
    r = requests.get(
        f"{BACKEND_HOST}/api/v1/public/vaccination/statuses/full/count/latest")
    full = r.json()["value"]

    r = requests.get(
        f"{BACKEND_HOST}/api/v1/public/vaccination/statuses/partial/count/latest"
    )
    partial = r.json()["value"]

    r = requests.get(f"{BACKEND_HOST}/api/v1/public/population/count/latest")
    population = r.json()["value"]

    r = requests.get(f"{BACKEND_HOST}/api/v1/public/vaccination/last_modified")
    last_modified = r.json()

    return partial / population, full / population, datetime.fromisoformat(
        last_modified)


def get_background_color(proportion):
    yellow_at = 50
    green_at = 90

    gradient_ry = Color("#ff8888").range_to(Color("gold"), yellow_at)
    gradient_yg = Color("gold").range_to(Color("lawngreen"),
                                         green_at - yellow_at)
    gradient = list(gradient_ry) + list(gradient_yg)
    i = min(int(round(proportion * 100)), green_at - 1)
    return gradient[i].hex


def get_answer(proportion):
    # Numbers from here:
    # https://meduza.io/feature/2021/11/24/v-avstrii-vaktsinirovali-65-zhiteley-i-vse-ravno-vveli-lokdaun-kak-zhe-tak-razve-nam-ne-obeschali-chto-etogo-tochno-hvatit-dlya-kollektivnogo-immuniteta
    if proportion > 0.95:
        return "Yes"
    if proportion > 0.85:
        return "Probably"
    if proportion > 0.83:
        return "Maybe"
    if proportion > 0.6:
        return "Not yet"
    return "No"


@app.route("/")
def are_we_there_yet():
    proportion_partial, proportion_full, date = get_proportion()
    print(date)
    print(type(date))

    return render_template(
        "percentage.html",
        answer=get_answer(proportion_full),
        bg_color=str(get_background_color(proportion_full)),
        percent_partial=f"{proportion_partial:,.2%}",
        percent_full=f"{proportion_full:,.2%}",
        date=date.strftime("%Y-%m-%d %H:%M"),
    )
